﻿using System;
using System.Collections.Generic;

namespace Solum.Data
{
	/// <summary>
	/// Common interface defining methods for all data objects.
	/// </summary>
	/// <typeparam name="T">The type of entity in the repository.</typeparam>
	public interface IRepository<T> : IDisposable where T : class
	{
		/// <summary>
		/// Adds a new entity to the repository.
		/// </summary>
		/// <param name="entity">The entity to add.</param>
		void Add(T entity);

		/// <summary>
		/// Determines whether the specified predicate can be found in the repository.
		/// </summary>
		/// <param name="where">The predicate to match against.</param>
		/// <returns>Whether a match could be found.</returns>
		bool IsDuplicate(Func<T, bool> where);

		/// <summary>
		/// Determines the number of entities in the repository.
		/// </summary>
		/// <returns>Number of entities in repository.</returns>
		int Count();

		/// <summary>
		/// Retrieves all entities in the repository.
		/// </summary>
		/// <returns>All repository entities.</returns>
		IEnumerable<T> GetAll();

		/// <summary>
		/// Retrieves an entity by its id.
		/// </summary>
		/// <param name="id">The id to find an entity by.</param>
		/// <returns>The entity found.</returns>
		T GetById(long id);

		/// <summary>
		/// Retrieves an entity based on the predicate.
		/// </summary>
		/// <param name="where">The predicate to find the entity with.</param>
		/// <returns>The matched entity.</returns>
		T Get(Func<T, bool> where);

		/// <summary>
		/// Retrieves many entities based on the predicate.
		/// </summary>
		/// <param name="where">The predicate to find entities with.</param>
		/// <returns>A list of the entities.</returns>
		IEnumerable<T> GetMany(Func<T, bool> where);

		/// <summary>
		/// Updates the specified entity.
		/// </summary>
		/// <param name="entity">The entity to update.</param>
		void Update(T entity);
		
		/// <summary>
		/// Deletes the specified entity.
		/// </summary>
		/// <param name="entity">The entity to delete.</param>
		void Delete(T entity);
		
		/// <summary>
		/// Deletes entites based on the predicate.
		/// </summary>
		/// <param name="where">The predicate to find entities to delete.</param>
		void Delete(Func<T, bool> where);
	}
}
