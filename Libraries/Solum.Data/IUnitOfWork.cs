﻿namespace Solum.Data
{
	/// <summary>
	/// Common interface for units of work.
	/// </summary>
	public interface IUnitOfWork
	{
		/// <summary>
		/// Commit the unit of work.
		/// </summary>
		void Commit();
	}
}
