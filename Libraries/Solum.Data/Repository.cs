﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace Solum.Data
{
	/// <summary>
	/// Common implementation of methods for all data objects.
	/// </summary>
	/// <typeparam name="T">Type of data to manage.</typeparam>
	public class Repository<T> : IRepository<T> where T : class
	{

		#region Declarations

		protected DbContext m_DbContext;
		
		#endregion

		#region Constructor
		/// <summary>
		/// Constructs a new repository instance.
		/// </summary>
		/// <param name="dbContext">The database context to use.</param>
		public Repository(DbContext dbContext)
		{
			m_DbContext = dbContext;
		}

		/// <summary>
		/// Destroys the repository instance.
		/// </summary>
		public void Dispose()
		{
			if (m_DbContext != null) { m_DbContext.Dispose(); }
		}

		#endregion

		#region Properties

		protected DbContext DbContext
		{
			get { return m_DbContext; }
		}

		protected DbSet<T> DbSet
		{
			get { return m_DbContext.Set<T>(); }
		}

		#endregion

		#region Methods

		public virtual void Add(T entity)
		{
			DbSet.Add(entity);
		}

		public bool IsDuplicate(Func<T, bool> where)
		{
			return DbSet.Where(where).Count() > 0;
		}

		public int Count()
		{
			return DbSet.Count();
		}

		public virtual IEnumerable<T> GetAll()
		{
			return DbSet;
		}

		public virtual T GetById(long id)
		{
			return DbSet.Find(id);
		}

		public virtual IEnumerable<T> GetMany(Func<T, bool> where)
		{
			return DbSet.Where(where);
		}

		public T Get(Func<T, bool> where)
		{
			return DbSet.Where(where).FirstOrDefault();
		}

		public virtual void Update(T entity)
		{
			DbSet.Attach(entity);
			m_DbContext.Entry(entity).State = EntityState.Modified;
		}

		public virtual void Delete(T entity)
		{
			DbSet.Remove(entity);
		}

		public void Delete(Func<T, bool> where)
		{
			IEnumerable<T> objects = DbSet.Where(where);

			foreach (T obj in objects)
			{
				DbSet.Remove(obj);
			}
		}
		
		#endregion

	}
}
