﻿using System;

namespace Solum.Types
{
	public static class DateFunctions
	{

		#region Date Functions
		public static string DateCompare(string pCompareDate, string pBaseDate)
		{
			if (!TypeFunctions.IsValidDate(pCompareDate) | !TypeFunctions.IsValidDate(pBaseDate))
			{
				return "Invalid";
			}
			else if (Convert.ToDateTime(pCompareDate) == Convert.ToDateTime(pBaseDate))
			{
				return "Same";
			}
			else if (Convert.ToDateTime(pCompareDate) > Convert.ToDateTime(pBaseDate))
			{
				return "After";
			}
			else
			{
				return "Before";
			}
		}

		public static string GetDate(System.DateTime pCurrentDate)
		{
			string lMonth = Months(pCurrentDate.Month, true);
			string lYear = TypeFunctions.CastStr(pCurrentDate.Year).PadLeft(4, '0');
			string lDay = TypeFunctions.CastStr(pCurrentDate.Day).PadLeft(2, '0');
			return lDay + " " + lMonth + " " + lYear;
		}

		public static string GetDateNow()
		{
			return GetDate(DateTime.Now);
		}
		#endregion

		#region Time Functions
		public static string GetDateTime(System.DateTime pCurrentDateTime, bool pGetSeconds)
		{
			return GetDate(pCurrentDateTime) + " " + GetTime(pCurrentDateTime, pGetSeconds);
		}

		public static string GetDateTimeNow(bool pGetSeconds)
		{
			System.DateTime lCurrentDateTime = DateTime.Now;
			return GetDateTime(lCurrentDateTime, pGetSeconds);
		}

		public static string GetTime(System.DateTime pCurrentTime, bool pGetSeconds)
		{
			string lHour = TypeFunctions.CastStr(pCurrentTime.Hour).PadLeft(2, '0');
			string lMinute = ":" + TypeFunctions.CastStr(pCurrentTime.Minute).PadLeft(2, '0');
			string lSecond = "";
			if (pGetSeconds)
				lSecond = ":" + TypeFunctions.CastStr(pCurrentTime.Second).PadLeft(2, '0');
			return lHour + lMinute + lSecond;
		}

		public static string GetTimeNow(bool pGetSeconds)
		{
			System.DateTime lCurrentTime = DateTime.Now;
			return GetTime(lCurrentTime, pGetSeconds);
		}
		#endregion

		#region Conversion Functions
		public static string Days(int pDayNo)
		{
			switch (pDayNo)
			{
				case 0:
					return "Sunday";
				case 1:
					return "Monday";
				case 2:
					return "Tuesday";
				case 3:
					return "Wednesday";
				case 4:
					return "Thursday";
				case 5:
					return "Friday";
				case 6:
					return "Saturday";
				default:
					return "DayNo Error - " + pDayNo;
			}
		}

		public static string Months(int pMonthNo, bool pShortMonth)
		{
			switch (pMonthNo)
			{
				case 1:
					return TypeFunctions.CheckForNull(pShortMonth, "Jan", "January");
				case 2:
					return TypeFunctions.CheckForNull(pShortMonth, "Feb", "February");
				case 3:
					return TypeFunctions.CheckForNull(pShortMonth, "Mar", "March");
				case 4:
					return TypeFunctions.CheckForNull(pShortMonth, "Apr", "April");
				case 5:
					return TypeFunctions.CheckForNull(pShortMonth, "May", "May");
				case 6:
					return TypeFunctions.CheckForNull(pShortMonth, "Jun", "June");
				case 7:
					return TypeFunctions.CheckForNull(pShortMonth, "Jul", "July");
				case 8:
					return TypeFunctions.CheckForNull(pShortMonth, "Aug", "August");
				case 9:
					return TypeFunctions.CheckForNull(pShortMonth, "Sep", "September");
				case 10:
					return TypeFunctions.CheckForNull(pShortMonth, "Oct", "October");
				case 11:
					return TypeFunctions.CheckForNull(pShortMonth, "Nov", "November");
				case 12:
					return TypeFunctions.CheckForNull(pShortMonth, "Dec", "December");
				default:
					return "MonthNo Error - " + pMonthNo;
			}
		}
		#endregion

	}
}
