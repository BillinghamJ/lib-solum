﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace Solum.Types
{
	public static class ImageFunctions
	{

		#region Upload Functions
		public static void UploadFile(string FileType, System.Web.UI.HtmlControls.HtmlInputFile ifFile, ref string FileName, ref Int32 FileLen, ref string FileMime, ref byte[] FileData)
		{
			try
			{
				FileLen = TypeFunctions.CastLng(ifFile.PostedFile.InputStream.Length);
				FileInfo fiFile = new FileInfo(ifFile.PostedFile.FileName);
				FileName = fiFile.Name;
				FileMime = ifFile.PostedFile.ContentType;
				Stream smStrm = ifFile.PostedFile.InputStream;
				switch (FileType)
				{
					case "Image":
						// upload the file into byte array
						byte[] myBytes = new byte[TypeFunctions.CastLng(FileLen) + 1];
						smStrm = ifFile.PostedFile.InputStream;
						smStrm.Read(myBytes, 0, TypeFunctions.CastLng(FileLen));
						FileData = myBytes;
						break;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
			}
		}
		#endregion

		#region Thumbnail Functions
		public static byte[] ResizeImage(System.Drawing.Image pOriginalImage, int pMaxLongDimension, int pMaxShortDimension)
		{
			// Work out the relative dimensions
			int lThumbnailWidth = 0;
			int lThumbnailHeight = 0;
			decimal lWidthProportion = default(decimal);
			decimal lHeightProportion = default(decimal);
			decimal lMinProportion = default(decimal);
			if (pOriginalImage.Width > pOriginalImage.Height)
			{
				lWidthProportion = (pMaxLongDimension / pOriginalImage.Width);
				lHeightProportion = (pMaxShortDimension / pOriginalImage.Height);
				lMinProportion = Math.Min(lWidthProportion, lHeightProportion);
				lThumbnailWidth = (int)(pMaxLongDimension * (lMinProportion / lWidthProportion));
				lThumbnailHeight = (int)(pMaxShortDimension * (lMinProportion / lHeightProportion));
			}
			else
			{
				lWidthProportion = (pMaxShortDimension / pOriginalImage.Width);
				lHeightProportion = (pMaxLongDimension / pOriginalImage.Height);
				lMinProportion = Math.Min(lWidthProportion, lHeightProportion);
				lThumbnailWidth = (int)(pMaxShortDimension * (lMinProportion / lWidthProportion));
				lThumbnailHeight = (int)(pMaxLongDimension * (lMinProportion / lHeightProportion));
			}
			// Create new images based on the size specified
			Bitmap lThumbnailImage = new Bitmap(lThumbnailWidth, lThumbnailHeight);
			Graphics e = Graphics.FromImage(lThumbnailImage);
			e.CompositingQuality = CompositingQuality.HighQuality;
			e.SmoothingMode = SmoothingMode.HighQuality;
			e.InterpolationMode = InterpolationMode.HighQualityBicubic;
			e.DrawImage(pOriginalImage, 0, 0, lThumbnailWidth + 1, lThumbnailHeight + 1);
			// Turn the new image back into a stream and then into an array of bytes
			MemoryStream lThumbnailStream = new MemoryStream();
			lThumbnailImage.Save(lThumbnailStream, ImageFormat.Jpeg);
			byte[] lThumbnailBytes = new byte[TypeFunctions.CastLng(lThumbnailStream.Length) + 1];
			lThumbnailStream.Position = 0;
			lThumbnailStream.Read(lThumbnailBytes, 0, TypeFunctions.CastLng(lThumbnailStream.Length));
			return lThumbnailBytes;
		}
		#endregion

	}
}
