﻿using System;

namespace Solum.Types
{
	public static class StringFunctions
	{

		#region Shortening Functions
		public static string ShortenText(string pText, int pMaxLength, string pTerminator)
		{
			if (pText.Length <= pMaxLength)
			{
				return pText;
			}
			else if (pMaxLength < pTerminator.Length)
			{
				return pText.Substring(0, pTerminator.Length);
			}
			else
			{
				return pText.Substring(0, pMaxLength - pTerminator.Length) + pTerminator;
			}
		}

		public static string NextLetter(string pValue)
		{
			int LowChr = (int)'a';
			int HighChr = (int)'z';
			if (pValue.Length > 0)
			{
				char Digit = Convert.ToChar(pValue.Substring(pValue.Length - 1, 1));
				if ((int)Digit != HighChr)
				{
					return pValue.Substring(0, pValue.Length - 1) + (char)((int)(char)(Digit) + 1);
				}
				else if (pValue.Length > 1)
				{
					return NextLetter(pValue.Substring(0, pValue.Length - 1)) + LowChr.ToString();
				}
				else
				{
					return LowChr.ToString() + LowChr.ToString();
				}
			}
			else
			{
				return LowChr.ToString();
			}
		}
		#endregion

		#region Match Functions
		public static string MatchInList(string pVal, string pVariableList, string pSelectedStr)
		{
			string ItemInList = "";
			string TestList = "";
			TestList = pVariableList + ",";
			while (TestList.Length > 0)
			{
				ItemInList = ExtractWord(ref TestList, ",");
				if (TypeFunctions.CastStr(ItemInList) == TypeFunctions.CastStr(pVal))
				{
					return pSelectedStr;
				}
			}
			return "";
		}

		public static string Match(string pVal, string pVariable, string pSelectedStr)
		{
			if (pVariable == pVal)
			{
				return pSelectedStr;
			}
			else
			{
				return "";
			}
		}
		#endregion

		#region Manipulation Functions
		public static string ExtractWord(ref string pPhrase, string pSearchStr)
		{
			int NextAmp = 0;
			if (pPhrase.Length > 0)
			{
				if (pPhrase.Substring(pPhrase.Length - 1, 1) != pSearchStr)
					pPhrase = pPhrase + pSearchStr;
			}
			NextAmp = pPhrase.ToUpper().IndexOf(pSearchStr.ToUpper());
			if (NextAmp == 0)
			{
				pPhrase = "";
				return "";
			}
			else if (NextAmp == 1)
			{
				pPhrase = pPhrase.Substring(pSearchStr.Length, pPhrase.Length - pSearchStr.Length).TrimStart();
				return ExtractWord(ref pPhrase, pSearchStr);
			}
			else
			{
				string Word = pPhrase.Substring(0, NextAmp - 1).TrimStart();
				pPhrase = pPhrase.Substring(NextAmp + pSearchStr.Length, pPhrase.Length - NextAmp - pSearchStr.Length + 1).TrimStart();
				return Word;
			}
		}

		public static string RemoveLeftWords(string pFileName, string pSeparator)
		{
			int PosOfLastSeparator = 0;
			PosOfLastSeparator = pFileName.IndexOf(pSeparator);
			if (PosOfLastSeparator > 0)
			{
				return pFileName.Substring(pFileName.Length - PosOfLastSeparator);
			}
			else
			{
				return pFileName;
			}
		}

		public static string RemoveRightWords(string pFileName, string pSeparator)
		{
			int PosOfLastSeparator = 0;
			PosOfLastSeparator = pFileName.LastIndexOf(pSeparator);
			if (PosOfLastSeparator > 0)
			{
				return pFileName.Substring(0, PosOfLastSeparator - 1);
			}
			else
			{
				return pFileName;
			}
		}

		public static string DivideList(string pList, string pSeparator, int pMaxLength)
		{
			string TempList = null;
			string Para = null;
			int LastSpacePos = 0;
			pList = pList + pSeparator;
			TempList = "";
			while (pList.Length > 0)
			{
				Para = ExtractWord(ref pList, "<br>");
				if (Para.Length <= pMaxLength)
				{
					TempList = TempList + Para + pSeparator;
				}
				else
				{
					while (Para.Length > 0)
					{
						if (Para.Length <= pMaxLength)
						{
							TempList = TempList + Para + pSeparator;
							Para = "";
						}
						else
						{
							LastSpacePos = Para.LastIndexOf(" ");
							if (LastSpacePos == 0)
							{
								TempList = TempList + Para.Substring(0, pMaxLength - 1) + pSeparator;
								Para = Para.Substring(pMaxLength + 1, Para.Length - pMaxLength);
							}
							else
							{
								TempList = TempList + Para.Substring(0, LastSpacePos - 1) + pSeparator;
								Para = Para.Substring(LastSpacePos + 1, Para.Length - LastSpacePos);
							}
						}
					}
				}
			}
			return TempList;
		}

		public static void ExtractWordsIntoArray(string pRoleString, string pSeparator, ref string[] pRoles)
		{
			while (pRoleString.Length > 0)
			{
				string NewRole = ExtractWord(ref pRoleString, ",");
				pRoles[pRoles.GetUpperBound(0)] = NewRole;
			}
		}
		#endregion

	}
}
