﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Solum.Types
{
	public static class TypeFunctions
	{

		#region System.Convert Functions
		public static DateTime CastDate(object value)
		{
			return System.Convert.ToDateTime(value);
		}

		public static bool CastBool(object value)
		{
			return System.Convert.ToBoolean(value);
		}

		public static string CastStr(object value)
		{
			return System.Convert.ToString(value);
		}

		public static DateTime CastDateTime(object value)
		{
			return System.Convert.ToDateTime(value);
		}

		public static decimal CastDec(object value)
		{
			return System.Convert.ToDecimal(value);
		}

		public static int CastInt(object value)
		{
			return System.Convert.ToInt16(value);
		}

		public static float CastSingle(object value)
		{
			return System.Convert.ToSingle(value);
		}

		public static Int32 CastInt32(object value)
		{
			return System.Convert.ToInt32(value);
		}

		public static Int32 CastLng(object value)
		{
			return System.Convert.ToInt32(value);
		}

		public static double CastDeg(double value)
		{
			return (value * 180) / Math.PI;
		}

		public static double CastRad(double value)
		{
			return (value * Math.PI) / 180;
		}

		public static short CastShort(object value)
		{
			return (short)System.Convert.ToSingle(value);
		}
		#endregion

		#region Formatting Functions
		public static object FormatDecimal(decimal pValue, int pDecimalPlaces, bool pGrouping)
		{
			string FinalValue = pValue.ToString();
			if (FinalValue.IndexOf(".") == -1)
				FinalValue = FinalValue + ".";
			int DecimalPointPos = FinalValue.IndexOf(".");
			int NumDigitsAfterDecimalPoint = FinalValue.Length - DecimalPointPos - 1;
			int NumDigitsBeforeDecimalPoint = DecimalPointPos;
			// Work out decimal places
			if (pDecimalPlaces == 0)
			{
				FinalValue = FinalValue.Substring(0, DecimalPointPos);
			}
			else if (FinalValue.Length >= DecimalPointPos + pDecimalPlaces + 1)
			{
				FinalValue = FinalValue.Substring(0, DecimalPointPos + pDecimalPlaces + 1);
			}
			// Sort out grouping
			if (pGrouping)
			{
				if (NumDigitsBeforeDecimalPoint > 3)
				{
					for (int GroupComma = 1; GroupComma <= Math.Truncate((double)(NumDigitsBeforeDecimalPoint / 3)); GroupComma++)
					{
						FinalValue = FinalValue.Insert(DecimalPointPos - (GroupComma * 3), ",");
					}
				}
			}
			return FinalValue;
		}

		public static string FormatDateType(System.DateTime pDate, string pType)
		{
			System.Text.StringBuilder DateString = new System.Text.StringBuilder();
			DateString.Append(pDate.Day.ToString().PadLeft(2, '0'));
			DateString.Append(" ");
			DateString.Append(DateFunctions.Months(pDate.Month, true));
			DateString.Append(" ");
			DateString.Append(pDate.Year.ToString().Substring(2));
			if (pType == "DateTime")
			{
				DateString.Append(" ");
				DateString.Append(pDate.Hour.ToString().PadLeft(2, '0'));
				DateString.Append(":");
				DateString.Append(pDate.Minute.ToString().PadLeft(2, '0'));
			}
			return DateString.ToString();
		}

		public static string FormatDateType(string pDate, string pType)
		{
			string DateString = FormatDate(pDate, 1, 11);
			return DateString;
		}

		public static string FormatDate(string pDate, int pStartPos, int pLen)
		{
			try
			{
				if ((pDate == null) | (pDate.Length == 0))
				{
					return "";
				}
				else if (pDate.Length < pStartPos + pLen)
				{
					return pDate.Substring(pStartPos);
				}
				else
				{
					return pDate.Substring(pStartPos, pLen);
				}
			}
			catch
			{
				return "";
			}
		}
		#endregion

		#region Translation Functions
		public static string TranslateNvlToCheck(string Data)
		{
			if (string.IsNullOrEmpty(Data))
			{
				return "off";
			}
			else
			{
				return "on";
			}
		}

		public static bool TranslateCheckToBoolean(string checkbx)
		{
			if (checkbx == "on")
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public static string TranslateBooleanToCheck(bool checkbx)
		{
			if (checkbx)
			{
				return "on";
			}
			else
			{
				return "off";
			}
		}

		public static int TranslateBooleanToBinary(bool @bool)
		{
			if (@bool == true)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}

		public static bool TranslateBinaryToBoolean(string binary)
		{
			if (binary == "1" | binary == "True")
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public static int DecodeBoolean(bool Value)
		{
			if (Value == true)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}

		public static string Opposite(string ValueType, string Value)
		{
			string OnValue = "";
			string OffValue = "";
			switch (ValueType)
			{
				case "Checkbox":
					OnValue = "on";
					OffValue = "off";
					break;
				case "Boolean":
					OnValue = "True";
					OffValue = "False";
					break;
			}
			if (Value == OnValue)
			{
				return OffValue;
			}
			else
			{
				return OnValue;
			}
		}

		public static string CStrToHtml(string OldString)
		{
			string NewStr = null;
			NewStr = OldString.Replace(Convert.ToChar(13).ToString(), "<br>");
			return NewStr;
		}

		public static string CHtmlToStr(string OldString)
		{
			string NewStr = null;
			NewStr = OldString.Replace("<br>", Convert.ToChar(13).ToString());
			NewStr = NewStr.Replace("<br />", Convert.ToChar(13).ToString());
			NewStr = NewStr.Replace("<h1>", "");
			NewStr = NewStr.Replace("</h1>", "");
			NewStr = NewStr.Replace("<h2>", "");
			NewStr = NewStr.Replace("</h2>", "");
			NewStr = NewStr.Replace("<p>", "");
			NewStr = NewStr.Replace("</p>", "");
			return NewStr;
		}

		public static string CStrToJS(string OldString)
		{
			string NewStr = null;
			NewStr = OldString.Replace("'", "\\'");
			return NewStr;
		}

		public static string cStrToURL(string OldString)
		{
			string NewStr = "";
			NewStr = OldString.Replace(" ", "+");
			NewStr = NewStr.Replace("&", "%26");
			return NewStr;
		}

		public static string cStrToSQL(string OldString)
		{
			string NewStr = "";
			NewStr = OldString.Replace("'", "''");
			NewStr = NewStr.Replace("=", "%EQ%");
			return NewStr;
		}

		public static string cSQLToStr(string OldString)
		{
			string NewStr = "";
			NewStr = OldString.Replace("''", "'");
			NewStr = NewStr.Replace("%EQ%", "=");
			return NewStr;
		}

		public static string SeparateWordsByHypen(string OldString)
		{
			Regex Regex = new Regex("[\\x00-\\x1F]|[\\x21-\\x2F]|[\\x3A-\\x40]|[\\x5B-\\x60]|[\\x7B-\\x7F]");
			string NewStr = Regex.Replace(OldString, "");
			NewStr = NewStr.Replace(" ", "-");
			return NewStr;
		}
		#endregion

		#region Data Validation Functions
		public static bool IsValidDateTime(string pDateTime)
		{
			try
			{
				if (pDateTime.Length != 17)
				{
					return false;
				}
				else if (!IsValidDate(pDateTime.Substring(0, 11)))
				{
					return false;
				}
				else if (pDateTime.Substring(11, 1) != " ")
				{
					return false;
				}
				else if (!IsValidTime(pDateTime.Substring(12, 5)))
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public static bool IsValidDateTimeWithSeconds(string pDateTime)
		{
			try
			{
				if (pDateTime.Length != 20)
				{
					return false;
				}
				else if (!IsValidDate(pDateTime.Substring(0, 11)))
				{
					return false;
				}
				else if (pDateTime.Substring(11, 1) != " ")
				{
					return false;
				}
				else if (!IsValidTimeWithSeconds(pDateTime.Substring(12, 8)))
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public static bool IsValidDate(string pDate)
		{
			try
			{
				if (pDate.Length != 11)
				{
					return false;
				}
				else if (!IsValidIntRange(pDate.Substring(0, 2), 1, 31))
				{
					return false;
				}
				else if ((pDate.Substring(2, 1) != " ") | (pDate.Substring(6, 1) != " "))
				{
					return false;
				}
				else if (!IsValidMonth(pDate.Substring(3, 3)))
				{
					return false;
				}
				else if (!IsValidIntRange(pDate.Substring(7, 4), 1950, 2050))
				{
					return false;
				}
				else if (Convert.ToDateTime(pDate) == Convert.ToDateTime(pDate))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public static bool IsValidTime(string pTime)
		{
			try
			{
				if (pTime.Length != 5)
				{
					return false;
				}
				else if (!IsValidIntRange(pTime.Substring(0, 2), 0, 23))
				{
					return false;
				}
				else if ((pTime.Substring(2, 1) != ":"))
				{
					return false;
				}
				else if (!IsValidIntRange(pTime.Substring(3, 2), 0, 59))
				{
					return false;
				}
				else if (Convert.ToDateTime(pTime) == Convert.ToDateTime(pTime))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public static bool IsValidTimeWithSeconds(string pTime)
		{
			try
			{
				if (pTime.Length != 8)
				{
					return false;
				}
				else if (!IsValidTime(pTime.Substring(0, 5)))
				{
					return false;
				}
				else if ((pTime.Substring(5, 1) != ":"))
				{
					return false;
				}
				else if (!IsValidIntRange(pTime.Substring(6, 2), 0, 59))
				{
					return false;
				}
				else if (Convert.ToDateTime(pTime) == Convert.ToDateTime(pTime))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public static bool IsValidMonth(string pMonth)
		{
			if (pMonth.Length != 3)
			{
				return false;
			}
			else
			{
				pMonth = pMonth.ToUpper();
				if ((pMonth != "JAN") & (pMonth != "FEB") & (pMonth != "MAR") & (pMonth != "APR") & (pMonth != "MAY") & (pMonth != "JUN") & (pMonth != "JUL") & (pMonth != "AUG") & (pMonth != "SEP") & (pMonth != "OCT") & (pMonth != "NOV") & (pMonth != "DEC"))
				{
					return false;
				}
			}
			return true;
		}

		public static bool IsValidBool(object pBool)
		{
			try
			{
				if (((pBool != null)) && (CastBool(pBool) == CastBool(pBool)))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public static bool IsValidInt(object pInt)
		{
			try
			{
				if (((pInt != null)) && (CastLng(pInt) == CastLng(pInt)))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public static bool IsValidIntRange(string pInt, int pLow, int pHigh)
		{
			try
			{
				if (((pInt != null)) && (CastLng(pInt) == CastLng(pInt)))
				{
					if (((CastInt(pInt) >= pLow) & (CastInt(pInt) <= pHigh)) | (pHigh < pLow))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public static bool IsValidDecimalRange(string pDec, decimal pLow, decimal pHigh)
		{
			try
			{
				if (((pDec != null)) && (pDec.Length > 0) && (CastDec(pDec) == CastDec(pDec)))
				{
					if ((CastDec(pDec) >= pLow) & (CastDec(pDec) <= pHigh))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public static bool IsValidEmailAddress(string pEmailAddress)
		{
			try
			{
				string Pattern = "^[a-zA-Z][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
				System.Text.RegularExpressions.Match DomainNameMatch = Regex.Match(pEmailAddress, Pattern);
				if (DomainNameMatch.Success)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public static bool IsValidDomain(string pDomain)
		{
			try
			{
				string Pattern = "^[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
				System.Text.RegularExpressions.Match DomainNameMatch = Regex.Match(pDomain, Pattern);
				if (DomainNameMatch.Success)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public static bool IsValidUrl(string pUrl)
		{
			try
			{
				string Pattern = "^((ht|f)tp(s?):\\/\\/|~/|/)?([\\w]+:\\w+@)?([a-zA-Z]{1}([\\w-]+.)+([\\w]{2,5}))(:[\\d]{1,5})?((/?\\w+/)+|/?)(\\w+.[\\w]{3,4})?((\\?\\w+=\\w+)?(&\\w+=\\w+)*)?";
				System.Text.RegularExpressions.Match DomainNameMatch = Regex.Match(pUrl, Pattern);
				if (DomainNameMatch.Success)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public static bool IsValidIPAddress(string pIPAddress)
		{
			try
			{
				if (((pIPAddress != null)) && (!string.IsNullOrEmpty(pIPAddress)))
				{
					string IP1 = StringFunctions.ExtractWord(ref pIPAddress, ".");
					if (((IP1 != null)) && (!string.IsNullOrEmpty(IP1)) && IsValidIntRange(IP1, 0, 255))
					{
						string IP2 = StringFunctions.ExtractWord(ref pIPAddress, ".");
						if (((IP2 != null)) && (!string.IsNullOrEmpty(IP2)) && IsValidIntRange(IP2, 0, 255))
						{
							string IP3 = StringFunctions.ExtractWord(ref pIPAddress, ".");
							if (((IP3 != null)) && (!string.IsNullOrEmpty(IP3)) && IsValidIntRange(IP3, 0, 255))
							{
								string IP4 = StringFunctions.ExtractWord(ref pIPAddress, ".");
								if (((IP4 != null)) && (!string.IsNullOrEmpty(IP4)) && IsValidIntRange(IP4, 0, 255))
								{
									if ((pIPAddress == null) | (string.IsNullOrEmpty(pIPAddress)))
									{
										return true;
									}
									else
									{
										return false;
									}
								}
								else
								{
									return false;
								}
							}
							else
							{
								return false;
							}
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public static bool IsValidPostCode(ref string pPostCode, ref string pPostCodeOut, ref string pPostCodeIn)
		{
			try
			{
				pPostCode = pPostCode.Replace(" ", "");
				pPostCode = pPostCode.ToUpper();
				string PostCode = pPostCode;
				PostCode = PostCode.Replace("B", "A");
				PostCode = PostCode.Replace("C", "A");
				PostCode = PostCode.Replace("D", "A");
				PostCode = PostCode.Replace("E", "A");
				PostCode = PostCode.Replace("F", "A");
				PostCode = PostCode.Replace("G", "A");
				PostCode = PostCode.Replace("H", "A");
				PostCode = PostCode.Replace("I", "A");
				PostCode = PostCode.Replace("J", "A");
				PostCode = PostCode.Replace("K", "A");
				PostCode = PostCode.Replace("L", "A");
				PostCode = PostCode.Replace("M", "A");
				PostCode = PostCode.Replace("N", "A");
				PostCode = PostCode.Replace("O", "A");
				PostCode = PostCode.Replace("P", "A");
				PostCode = PostCode.Replace("Q", "A");
				PostCode = PostCode.Replace("R", "A");
				PostCode = PostCode.Replace("S", "A");
				PostCode = PostCode.Replace("T", "A");
				PostCode = PostCode.Replace("U", "A");
				PostCode = PostCode.Replace("V", "A");
				PostCode = PostCode.Replace("W", "A");
				PostCode = PostCode.Replace("X", "A");
				PostCode = PostCode.Replace("Y", "A");
				PostCode = PostCode.Replace("Z", "A");
				PostCode = PostCode.Replace("0", "9");
				PostCode = PostCode.Replace("1", "9");
				PostCode = PostCode.Replace("2", "9");
				PostCode = PostCode.Replace("3", "9");
				PostCode = PostCode.Replace("4", "9");
				PostCode = PostCode.Replace("5", "9");
				PostCode = PostCode.Replace("6", "9");
				PostCode = PostCode.Replace("7", "9");
				PostCode = PostCode.Replace("8", "9");
				if (PostCode == "A99AA")
				{
					pPostCodeOut = pPostCode.Substring(0, 2);
					pPostCodeIn = pPostCode.Substring(2, 3);
				}
				else if (PostCode == "A999AA")
				{
					pPostCodeOut = pPostCode.Substring(0, 3);
					pPostCodeIn = pPostCode.Substring(3, 3);
				}
				else if (PostCode == "AA99AA")
				{
					pPostCodeOut = pPostCode.Substring(0, 3);
					pPostCodeIn = pPostCode.Substring(3, 3);
				}
				else if (PostCode == "AA999AA")
				{
					pPostCodeOut = pPostCode.Substring(0, 4);
					pPostCodeIn = pPostCode.Substring(4, 3);
				}
				else if (PostCode == "A9A9AA")
				{
					pPostCodeOut = pPostCode.Substring(0, 3);
					pPostCodeIn = pPostCode.Substring(3, 3);
				}
				else if (PostCode == "AA9A9AA")
				{
					pPostCodeOut = pPostCode.Substring(0, 4);
					pPostCodeIn = pPostCode.Substring(4, 3);
				}
				else
				{
					return false;
				}
				pPostCode = pPostCodeOut + " " + pPostCodeIn;
				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}
		#endregion

		#region Null Value Functions
		public static string nvlStr(string ValueToCheck, string DefaultValue)
		{
			if ((ValueToCheck.Length == 0) | (ValueToCheck == null))
			{
				return DefaultValue;
			}
			else
			{
				return ValueToCheck;
			}
		}

		public static int nvlLng(long ValueToCheck, long DefaultValue)
		{
			return CastLng(nvlStr(CastStr(ValueToCheck), CastStr(DefaultValue)));
		}

		public static string CheckForNull(bool NullIndicator, string NullStr, string NotNullStr)
		{
			if (NullIndicator)
			{
				return NullStr;
			}
			else
			{
				return NotNullStr;
			}
		}
		#endregion

		#region JavaScript Functions
		public static string EncodeURL(string pUrl)
		{
			string lEncodedURL = pUrl.Replace("%", "%%*");
			return lEncodedURL;
		}

		public static string DecodeURL(string pEncodedUrl)
		{
			string lDecodedURL = pEncodedUrl.Replace("%%*", "%");
			return lDecodedURL;
		}
		#endregion

		#region General Functions
		public static string CheckForTrue(bool Indicator, string TrueStr, string FalseStr)
		{
			if (Indicator)
			{
				return TrueStr;
			}
			else
			{
				return FalseStr;
			}
		}

		public static string DisplayItemsInCollection(IList<int> Coll1)
		{
			string OutputStr = "";
			OutputStr = "Number of items = " + Coll1.Count;
			foreach (object i in Coll1)
			{
				//OutputStr += Coll1(i).Name
			}
			return OutputStr;
		}

		public static bool CheckForValueInCollection(string ColType, string Val, IList<int> Coll)
		{
			IList<int> Coll2 = new List<int>();
			foreach (object x in Coll)
			{
				if (ColType == "ASPUpload")
				{
					//If Instr(x.Name, Val) Then 
					//  Return True
					//  Exit For
					//End If
				}
				else
				{
					//If Instr(x, Val) Then 
					//  Return True
					//Exit For
					//End If
				}
			}
			return false;
		}
		#endregion

	}
}
