﻿using System;
using System.Diagnostics;
using Microsoft.Win32;

public static class RegistryFunctions
{

	#region Public Enums
	public enum RegistryTypes : int
	{
		ClassesRoot = 1,
		CurrentConfig = 2,
		CurrentUser = 3,
		DynamicData = 4,
		LocalMachine = 5,
		PerformanceData = 6,
		Users = 7
	}
	#endregion

	#region Registry Functions
	public static string GetRegistryValue(RegistryTypes pRegistry, string pFolder, string pRegistryKey, string pDefaultValue)
	{
		RegistryKey MyRegistryKey = SetUpRegistryString(pRegistry, pFolder);
		try
		{
			return MyRegistryKey.GetValue(pRegistryKey, pDefaultValue).ToString();
		}
		catch (Exception ex)
		{
			Debug.Print("GetRegistryValue-" + ex.Message);
			return "";
		}
		finally
		{
			MyRegistryKey.Close();
		}
	}

	public static void SetRegistryValue(RegistryTypes pRegistry, string pFolder, string pRegistryKey, string pValue)
	{
		RegistryKey MyRegistryKey = SetUpRegistryString(pRegistry, pFolder);
		try
		{
			MyRegistryKey.SetValue(pRegistryKey, pValue);
		}
		catch (Exception ex)
		{
			Debug.Print("SetRegistryValue-" + ex.Message);
		}
		finally
		{
			MyRegistryKey.Close();
		}
	}
	#endregion

	#region Supporting Functions
	private static void CreateRegistryFolder(RegistryKey pRegistry, string pFolder)
	{
		if (pRegistry.OpenSubKey(pFolder, true) == null)
		{
			RegistryKey NewRegistryKey = null;
			NewRegistryKey = pRegistry.CreateSubKey(pFolder, RegistryKeyPermissionCheck.ReadWriteSubTree);
			CreateRegistryFolder(pRegistry, pFolder);
		}
	}

	private static RegistryKey SetUpRegistryString(RegistryTypes pRegistry, string pFolder)
	{
		RegistryKey MyRegistryKey = null;
		switch (pRegistry)
		{
			case RegistryTypes.ClassesRoot:
				MyRegistryKey = Microsoft.Win32.Registry.ClassesRoot;
				break;
			case RegistryTypes.CurrentConfig:
				MyRegistryKey = Microsoft.Win32.Registry.CurrentConfig;
				break;
			case RegistryTypes.CurrentUser:
				MyRegistryKey = Microsoft.Win32.Registry.CurrentUser;
				break;
			case RegistryTypes.DynamicData:
				MyRegistryKey = Microsoft.Win32.Registry.PerformanceData;
				break;
			case RegistryTypes.LocalMachine:
				MyRegistryKey = Microsoft.Win32.Registry.LocalMachine;
				break;
			case RegistryTypes.Users:
				MyRegistryKey = Microsoft.Win32.Registry.Users;
				break;
			default:
				MyRegistryKey = Microsoft.Win32.Registry.CurrentUser;
				break;
		}
		CreateRegistryFolder(MyRegistryKey, pFolder);
		return MyRegistryKey.OpenSubKey(pFolder, true);
	}
	#endregion

}
