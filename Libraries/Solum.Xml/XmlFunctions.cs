﻿using System;
using System.IO;
using System.Xml;

namespace Solum.Xml
{
	public static class XMLFunctions
	{

		#region Supporting Functions - XML
		public static FileStream LockXMLFile(string pFileName)
		{
			FileStream XmlFileStream = null;
			while ((true))
			{
				try
				{
					if (!System.IO.File.Exists(pFileName))
					{
						XmlFileStream = new FileStream(pFileName, FileMode.CreateNew, FileAccess.Write, FileShare.None);
						XmlFileStream.Close();
					}
					XmlFileStream = new FileStream(pFileName, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
					break;
				}
				catch (Exception e)
				{
					System.Threading.Thread.Sleep(300);
				}
			}
			return XmlFileStream;
		}

		public static void CreateXMLDocumentAndRoot(string pRootName, ref XmlDocument pXmlDoc, ref XmlElement pXmlRoot)
		{
			pXmlDoc = new XmlDocument();
			XmlNode xmlDeclaration = pXmlDoc.CreateNode(XmlNodeType.XmlDeclaration, "", "");
			pXmlDoc.AppendChild(xmlDeclaration);
			pXmlRoot = pXmlDoc.CreateElement(pRootName);
			pXmlDoc.AppendChild(pXmlRoot);
		}

		public static XmlNode CreateXMLNode(ref XmlDocument pXmlDoc, ref XmlNode pXmlParentNode, string pName, string pValue)
		{
			XmlNode NewXmlNode = default(XmlNode);
			NewXmlNode = pXmlDoc.CreateNode(XmlNodeType.Element, pName, null);
			NewXmlNode.InnerText = pValue;
			pXmlParentNode.AppendChild(NewXmlNode);
			return NewXmlNode;
		}

		public static XmlNode CreateXMLNode(ref XmlDocument pXmlDoc, ref XmlElement pXmlParentNode, string pName, string pValue)
		{
			XmlNode NewXmlNode = default(XmlNode);
			NewXmlNode = pXmlDoc.CreateNode(XmlNodeType.Element, pName, null);
			NewXmlNode.InnerText = pValue;
			pXmlParentNode.AppendChild(NewXmlNode);
			return NewXmlNode;
		}

		public static void CreateXMLAttribute(ref XmlDocument pXmlDoc, ref XmlElement pXmlNode, string pName, string pValue)
		{
			XmlAttribute NewXmlAtt = pXmlDoc.CreateAttribute(pName);
			NewXmlAtt.InnerText = pValue;
			pXmlNode.Attributes.Append(NewXmlAtt);
		}

		public static void CreateXMLAttribute(ref XmlDocument pXmlDoc, ref XmlNode pXmlNode, string pName, string pValue)
		{
			XmlAttribute NewXmlAtt = pXmlDoc.CreateAttribute(pName);
			NewXmlAtt.InnerText = pValue;
			pXmlNode.Attributes.Append(NewXmlAtt);
		}
		#endregion

	}
}
